var scheduler = require('./scheduler')
var moment = require('moment')
var async = require('async');
var sns = require('./sns');
var ObjectID = require('mongodb').ObjectID;

var topicsAvailable;
// Update topics available
sns.listTopics({}, (err, data)=> {
  topicsAvailable = data.Topics;
});

console.log(new Date())

var api = {
  // View pushes without looking into the topics
  viewPushes: function(req, res){
    var returnedJobs = [];
    scheduler.agenda.jobs({}, function(err, jobs) {
      // Work with jobs (see below)
      returnedJobs = jobs.map((job)=>{
        return job.attrs
      })
      res.send(returnedJobs)
    })
  },

  // View push notification jobs (topic, how many users are registered to topic, time, message)
  refresh: function(req, res){
    console.log("viewing pushes")
    var topics, returnedTopics = {}, returnedJobs = [];
    scheduler.agenda.jobs({}, function(err, jobs) {
      // Work with jobs (see below)
      returnedJobs = jobs.map((job)=>{
        return job.attrs
      })
    });
    // get all sns topics
    sns.listTopics({}, (err, data)=> {
      if(err)
        return;
      topicsAvailable = data.Topics;
      // Get subscriptions
      async.each(topicsAvailable, (topic, cb)=> {
        sns.listSubscriptionsByTopic(topic, (err_topic, data_topic)=>{
          if(err_topic)
            return;
          returnedTopics[topic.TopicArn] = {
            subCount: data_topic.Subscriptions.length
          };
          cb();
          console.log(`${topic.TopicArn}: ${data_topic.Subscriptions.length}`)
        })
      }, function(err){
        if(err){
          console.log("failed to get all topics")
        }else {
          res.send({returnedTopics: returnedTopics, returnedJobs: returnedJobs});
        }

      })
    })
  },

  // Schedule push notification job for a given SNS topic at a given time.
  // Message and payload should be customizable.
  schedulePush: function(req, res){
    console.log(req.body)
    sns.createTopic({Name: req.body.topic}, (att)=> console.log(att))
    scheduler.agenda.schedule(new Date(req.body.date), 'schedule message', {message: req.body.message, topic: req.body.topic}, (err, job)=>{
      res.send(job)
    });
  },

  //  Delete push notification job
  deletePush: function(req, res){
    console.log(req.params)
    scheduler.agenda.cancel({_id: new ObjectID(req.params.id)}, function(err, numRemoved) {
      if(err){
        res.send(err)
      }else{
        res.send({removed: numRemoved})
      }

    });
  },
}





module.exports = api;