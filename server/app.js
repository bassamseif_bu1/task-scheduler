var express = require('express');
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser')
var app = express();
var api = require('./api')


// Serve static assets
app.use(express.static(path.resolve(__dirname, '..', 'build')));
app.use(bodyParser());


// Always return the main index.html, so react-router render the route in the client
app.get('/app', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
});

app.get('/api/pushes/refresh', api.refresh)
app.get('/api/pushes/view', api.viewPushes)
app.post('/api/pushes/schedule', api.schedulePush)
app.delete('/api/pushes/:id', api.deletePush)




module.exports = app;