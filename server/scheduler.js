var Agenda = require('agenda')
var agendaUI = require('agenda-ui');
var sns = require('./sns');
var scheduler = {};
var mongoConnectionString = "mongodb://127.0.0.1/agenda";
var agenda = scheduler.agenda = new Agenda({db: {address: mongoConnectionString}, processEvery: '2 seconds'});

agenda.define('schedule message', function(job, done) {
  console.log("job", job.attrs.data)
  sns.publish({Message: job.attrs.data.message, TopicArn: `arn:aws:sns:us-west-2:947523561665:${job.attrs.data.topic}`}, (err, res)=>{
    if(err)
      console.log(err)
    else{
      console.log("Published Message:", res)
    }
  })
  job.remove();
  done();
});

agenda.on('ready', function() {
  agenda.start();
});


// In case it fails while executing
function failGracefully() {
  agenda.stop(() => process.exit(0));
}

process.on('SIGTERM', failGracefully);
process.on('SIGINT', failGracefully);


module.exports = scheduler;