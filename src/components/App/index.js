import React, { Component } from 'react';
import classnames from 'classnames';
import Datetime from 'react-datetime'
import 'react-datetime/css/react-datetime.css';
import axios from 'axios';
import _ from 'lodash';
import 'bootstrap/dist/css/bootstrap.css'
var Button = require('react-bootstrap/lib/Button');
var Col = require('react-bootstrap/lib/Col');

var topicPrefix = "arn:aws:sns:us-west-2:947523561665:"

import './style.css';

class App extends Component {

  constructor(props, context) {
    super(props, context);

    // Initial view
    axios.get(`/api/pushes/refresh`)
      .then(res => {
        this.setState({status: "", topics: res.data.returnedTopics, pushes: res.data.returnedJobs})
      });

    this.state = {
      message: "",
      topic: "default",
      status: "Fetching data...",
      date: new Date(),
      pushes: [],
      topics: {},
    };

  };


  scheduleClick(){
    axios.post(`/api/pushes/schedule`, {
      message: this.state.message,
      date: this.state.date,
      topic: this.state.topic,
    })
      .then(res => {
        this.state.pushes.push(res.data)
        this.setState({
          pushes: this.state.pushes
        })
      });
  };
  viewClick(){
    this.setState({status: "Fetching pushes..."})
    axios.get(`/api/pushes/view`)
      .then(res => {
        console.log("data", res.data)
        this.setState({status: "", pushes: res.data})
      });
  };
  refreshClick(){
    this.setState({status: "Fetching pushes..."})
    axios.get(`/api/pushes/refresh`)
      .then(res => {
        this.setState({status: "", topics: res.data.returnedTopics, pushes: res.data.returnedJobs})
      });
  };
  deleteScheduledPush(id){
    axios.delete(`/api/pushes/${id}`)
      .then(res => {
        if(res.data.removed==1){
          _.remove(this.state.pushes, (push)=>{
            return push._id==id;
          })
          this.setState({pushes: this.state.pushes})
        }
      });
  };


  updateValue(param, value){
    this.state[param]= value;
  }




  render() {
    var styleWidth = {width: 400, margin: "auto"};

    var pushes = this.state.pushes.map((push, i)=>{
      return <div className="well" key={i}>
        <div><b>Message:</b> {push.data.message} </div>
        <div><b>Topic:</b> {push.data.topic} ({this.state.topics[topicPrefix + push.data.topic].subCount} subscribers) </div>
        <div><b>Send at:</b> {new Date(push.nextRunAt).toString()})</div>
        <Button  bsStyle="danger" onClick={()=> this.deleteScheduledPush(push._id)}>Remove</Button>
        </div>
    })
    var topics = Object.keys(this.state.topics).map((topic)=>{
      return <div  className="well" key={topic}>
      <div><b>Topic Name:</b>{topic} </div>
      <div><b>Subscribers Count:</b> {this.state.topics[topic].subCount}</div>
      </div>
    })
    return (
      <Col className="App">
        <div>
          <div>Topic:</div>
          <div>
            <input style={styleWidth} defaultValue={this.state.topic} onChange={(evt)=>{this.updateValue("topic",  evt.target.value)}}/>
          </div>
          <div>Message:</div>
          <div>
            <input style={styleWidth}  defaultValue={this.state.message} onChange={(evt)=>{this.updateValue("message",  evt.target.value)}}/>
          </div>
          <div>Scheduled Date:</div>
          <div style={styleWidth}><Datetime  defaultValue={this.state.date} onChange={(moment)=>{this.updateValue("date",  moment)}}/></div>
          <Button  bsStyle="primary"  onClick={this.scheduleClick.bind(this)}>Schedule</Button>
        </div>
        <div>
          <Button bsStyle="success" onClick={this.viewClick.bind(this)}>Refresh scheduled pushes only</Button>
          <Button bsStyle="success" onClick={this.refreshClick.bind(this)}>Refresh scheduled pushes & topics</Button>
          <div>{this.state.status}</div>
          <br/>
          <Col md={6}>
            <div><b>Pushes:</b></div>
            <div>{pushes}</div>
          </Col>
          <Col md={6}>
            <div><b>Topics:</b></div>
            <div>{topics}</div>
          </Col>
        </div>
      </Col>
    );
  }
}

export default App;
